// [SECTION] Comparisom QUery Operators

	// $gt/$gte operator
	/*
		Allows us to find documents that have field number values greater than or equal to a specified value.

		Syntax:
		db.collectionName.find({field : {$gt: value}});
	*/

	db.users.find({age: {$gt: 50}}); //greater than
	db.users.find({age: {$gte: 50}}); //greater than or equal

	// $lt/$lte operator
r
	/*
		Allows us to find documents that have field number values lesser than or equal to a specified value.

		Syntax:
		db.collectionName.find({field : {$lt: value}});
	*/

	db.users.find({age: {$lt: 50}}); //lesser than
	db.users.find({age: {$lte: 50}}); // lesser than or equal

	// $ne operator
	/*
		Allows us to find documents that have field number values not equal to a specified value

		Syntax:
		db.collectionName({field: {$ne: value}});
	*/

	db.users.find({ age: {$ne: 82}});

	// $in operator
	/*
		Allows us to fu=ind documents with specific match criteria using different values

		Syntax:
		db.collectionName.find({field: {$in: value}});
	*/

	db.users.find({lastName: {$in:["Hawking","Doe"]}});
	db.users.find({courses: {$in:["HTML","React"]}});

// [SECTION] Logical QUery Operators

	// $or operator
	/*
		Allows us to find documents that match a single crietia from multiple provided search criteria

		Syntax:
		db.collectionName.find({$or: [{fieldA: valueA},{fieldB: valueB}]});
	*/

	db.users.find({$or: [{firstName: "Neil"},{age: 25}]});
	db.users.find({$or: [{firstName: "Neil"},{age: {$gt: 30}}]});

	// $and operator
	/*
		Allows us to find documents matching multiple criteria in a single field

		Syntax:
		db.collectionName.find({$and: [{fieldA: valueA},{fieldB: valueB}]});

	*/

	db.users.find({$and: [{age: {$ne: 82}},{age: {$ne: 76}}]});

// [SECTION] Field Projection

	// Inclusion
	/*
		Allows us to include specific field/s when retrieving documents.
		The value provided is 1 to denote that the field is being included

		Syntax:
		db.collectionName.find({criteria},{field: 1});
	*/

	db.users.find(
		{
			firstName: "Jane"
		},
		{
			firstName: 1,
			lastName: 1,
			connect: 1
		}
	);

	// Exclusion
	/*
		> Allows us to exclude specific field/s when retrieving documents.
		> The value provided is 0 to denote that the field is being excluded

		Syntax:
		db.collectionName.find({criteria},{field: 0});
	*/

	db.users.find(
		{
			firstName: "Jane"
		},
		{
			connect: 0,
			department: 0
		}
	);

	// Suppresing the ID field
	/*
		> Allows us to exclude the "_id" when retrieving documents
		> When using the field projection, field inclusion and exclusion cannot be used at the same time
		> The only exception is excluding the "_id"
	*/

	db.users.find(
		{
			firstName: "Jane"
		},
		{
			firstName: 1,
			lastName: 1,
			connect: 1,
			_id: 0
		}
	);

	// Returning specific fields in Embedded documents

	db.users.find(
	    {
	        firstName: "Jane"
	    },
	    {
	        firstName: 1,
	        lastName: 1,
	        "connect.phone": 1
	    }
	);

	// Mini activity: Return all fields except the phone field embedded in contact field.

	db.users.find(
	    {
	        firstName: "Jane"
	    },
	    {
	        "connect.phone": 0
	    }
	);

	// Evaluate Query Operators
	/*
		$regex operator
			Allows us to find documents that match a specific pattern using regular expressions.

			Syntax:
			db.collectionName.find({field: {$regex: "pattern", $option: "$optionValue"}});
	*/

	// Case sensitive query
	db.users.find({firstName: {$regex: "N"}});

	// Case insensitive query
	db.users.find({firstName: {$regex: "j",$options:"$i"}});